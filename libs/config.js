/* libs/config */
var fs = require('fs');
module.exports = function(json_file) {
	return JSON.parse(fs.readFileSync(json_file));
}
