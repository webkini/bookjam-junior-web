/* libs/utils */

var OS = require('os');
var MD5 = require('js-md5');

module.exports = {

  getResponse: function(body) {
		try {
			body = (typeof body === 'string') ? JSON.parse(body) : body;
			if (!body.code) body.code = 200;
			return body;
		} catch(e) { return {}; }
	},

  getRequestHeaders: function(req, uhs) {
		return this.mergeObjects({
			'Content-Type': 'application/json',
			'App-Identifier': req.CONFIG.app.id,
			'App-Version': req.CONFIG.app.version,
			'Device-Identifier': 'http-node://anonymous',
			'Device-Type': 'ipad-retina',
			'OS-Version': OS.platform() + '_' + OS.release(),
			'SBML-Version': req.CONFIG.app.sbml_version,
			'Domain-Key': '',
			'Session-Key': '',
			'Device-Timestamp': Math.floor(Date.now() / 1000),
			'Access-Token': ''
		}, uhs);
	},

    getDeviceId: function(req) {
        var device_id = 'http-node://anonymous';
        device_id = (req) ? 'http-node://' + req.ip : device_id;
        device_id = (req.session.user) ? 'http-node://' + MD5(req.session.user.id) : device_id;
        return device_id;
    },

  //------

    mergeObjects: function(a,b) { for (var k in b) a[k] = b[k]; return a; }

}
