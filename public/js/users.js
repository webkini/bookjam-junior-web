/* /js/users.js */

var _users = function(){

    var self = this;

    function _request(api, method, body, callback, async) {
        async = async || true;
        body = (!body) ? '' : JSON.stringify(body);
        $.ajax({ url: api, type: method, dataType: 'JSON', contentType: 'application/json', data: body, success: callback, async: async });
    }

// public

    self.login = function(userData, callback) {
        return _request('/users/login', 'POST', userData, function(res) {
          Cookies.remove('auth_token');
          if (res.auth_token) Cookies.set('auth_token', res.auth_token, 3/24); //3hours
          if (callback) callback(res);
        });
    }

    self.logout = function() {
        Cookies.remove('auth_token');
        _request('/users/logout', 'DELETE', null, function(){ goUrl('/'); });
    }

    self.register = function(userData, callback) {
        return _request('/users/register', 'POST', userData, callback);
    }

    self.getMemberships = function(callback) {
        return _request('/users/memberships', 'GET', null, callback);
    }

    self.getPurchases = function(callback) {
        return _request('/users/purchases', 'GET', null,  callback);
    }

    self.getBooks = function(id, callback) {
        return _request('/products/books/'+id, 'GET', null, callback);
    }

    self.getCovers = function(id, callback) {
        return _request('/covers/'+id, 'GET', null, callback);
    }

    self.getProducts = function(callback) {
        return _request('/products/list', 'GET', null, callback);
    }

};

var Users = new _users;
