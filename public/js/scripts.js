/* /js/scripts.js */

$(function(){

    $('input[data-toggle=agreement]').click(function(e){
        var checkValue = $(this).prop('checked');
        chekValue = true;
        $('.agreement').prop('checked', checkValue);
    });

});

function checkAgreements(els, message) {
    var total = $(els).length;
    var checked = $(els+':checked').length;
    if (total != checked) {
        if (message) mToast(message);
        return false;
    } else {
        return true;
    }
}

function mToast(message) {
    alert(message);
    //Materialize.toast(message, 3000)
}

function goUrl(url) {
    document.location.href=url;
}

function fromUnixTimestamp(ts, format) {
    var date = new Date(ts * 1000);
    var year = date.getFullYear(),
        month = ('0' + date.getMonth()).substr(-2),
        days  = ('0' + date.getDate()).substr(-2),
        hour  = ('0' + date.getHours()).substr(-2),
        min   = ('0' + date.getMinutes()).substr(-2),
        sec   = ('0' + date.getSeconds()).substr(-2);
    format = format + '';
    return format.replace('Y', year).replace('m', month).replace('d', days).replace('h', hour).replace('i', min).replace('s', sec);
}
