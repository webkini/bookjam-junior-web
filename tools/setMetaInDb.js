var sqlite = require('sqlite3').verbose();
var request = require('request');

var JUNIOR_DB = new sqlite.Database('../data/junior.sqlite');
var MARKET_HOST = 'http://store.bookjam.pe.kr/api/v2';
var ITEMS = [], SERIES = [];

var collectItems = function(callback){
    JUNIOR_DB.all("SELECT * FROM products", function(err, rows){
        var item_ids = [];
        rows.forEach(function(row){
            item_ids = item_ids.concat(row.items.split(','));
        });
        item_ids = item_ids.filter(getUnique);

        request({
            url: MARKET_HOST + '/items',
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            json: { "items": item_ids }
        }, function(err, reso, body){
            if (!err) {
                var items = body.items;
                var series_ids = [];

                JUNIOR_DB.serialize(function(){
                    /* insert items data */
                    var stmt = JUNIOR_DB.prepare("INSERT INTO items VALUES (?,?,?)");
                    for (var id in items) {
                        var v = {
                            'id': id,
                            'series': (items[id].series)?items[id].series[0]:'',
                            'meta': JSON.stringify(items[id])
                        }
                        //stmt.run(v.id, v.series, v.meta);
                        if (v.series!='') series_ids.push(v.series);
                        console.log(id);
                    }
                    console.log('insert finalizing...(takes several minutes)');
                    stmt.finalize();
                    console.log('insert items data done.');

                    series_ids = series_ids.filter(getUnique);
                    collectSeries(series_ids);

                    //JUNIOR_DB.close();
                });
                //console.log(series_ids);

            }
        });

        //callback(item_ids);
    });
};

var collectSeries = function(ids) {
    request({
        url: MARKET_HOST + '/series',
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        json: { "series": ids }
    }, function(err, reso, body){
        if (!err) {
            var series = body.series;
            JUNIOR_DB.serialize(function(){
                var stmt = JUNIOR_DB.prepare("INSERT INTO series VALUES (?,?)");
                for (var id in series) {
                    var v = {
                        'id': id,
                        'meta': JSON.stringify(series[id])
                    }
                    stmt.run(v.id, v.meta);
                    console.log(id);
                }
                console.log('insert finalizing...(takes several minutes)');
                stmt.finalize();
                console.log('insert series data done.');
            });
        }
    });
}

function getUnique(v, i, self) { return self.indexOf(v) === i; }

function escapeJSON(unsafe) {
    return unsafe
        .replace(/\\n/g, "\\n")
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
 }

/* MAIN */

collectItems();
