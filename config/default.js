module.exports = {
	"debug": true,

	"app": {
		"id": "net.bookjam.Junior",
		"version": "0.0.1",
		"sbml_version": "2.0.0"
	},

	"host": {
		"api": {
			"cloud": "https://bookjam-cloud-test.appspot.com/api/v2.2",
			"market": "https://store.bookjam.pe.kr/api/v2"
		},
		"cds": {
			"bxps": "http://bxfs152.bookjam.net:20128"
		}
	},

	"db": {
		"catalog": "data/junior.sqlite"
	},

	"auth": {
		"algorithm": "aes-256-ctr",
		"password": "aksskf16aksskf",
		"cookie": {
			"expires": 0.125,
			"remember_days": 90
		},

		"facebook": {
		}
	}
};
