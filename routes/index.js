var express = require('express');
var router = express.Router();

var Showcases = require('../controllers/showcases');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/memberships', function(req, res, next) {
    res.render('memberships', { title: 'Express' });
});

router.get('/support', function(req, res, next) {
    res.render('support', { title: 'Express' });
});

router.get('/community', function(req, res, next) {
  res.render('community');
});

router.get('/products/books/:id', Showcases.products.books);
router.get('/products/list', Showcases.products.list);
router.get('/covers/:item_id', Showcases.market.covers);

module.exports = router;
