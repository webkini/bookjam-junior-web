var express = require('express');
var router = express.Router();

var USERS = require('../controllers/users');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/login', function(req, res, next) { res.render('users/login'); });
router.post('/login', USERS.log.in);
router.get('/logout', USERS.log.out).delete('/logout', USERS.log.out);

router.get('/register', function(req, res, next) { res.render('users/register'); });
router.post('/register', USERS.register);

router.get('/password', function(req, res, next) { res.render('users/password'); });

router.get('/mypage', function(req, res, next) { res.render('users/mypage'); })
router.get('/memberships', USERS.get.memberships);
router.get('/purchases', USERS.get.purchases);

module.exports = router;
