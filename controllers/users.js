/* controllers/users */

var crypto = require('crypto');

var UTILS = require('../libs/utils');
var CLOUD = require('../libs/cloud');

var self = module.exports = {

  session: {
      set: function(req, user) {
          return req.session.user = {
              "id": user.user_id,
              "key": user.session_key,
              "name": user.name,
              "email": user.email
          };
      },

      get: function(req) { return (req.session.user) ? req.session.user : null; },

      remove: function(req) { delete req.session.user; }
  },

  log: {
      in: function(req, res, next) {
        var userData = {
            'channel': req.body.channel || 'bookjam',
            'email': req.body.email || '',
            'password': req.body.password || '',
            'external_id': req.body.external_id || '',
            'access_token': req.body.access_token || ''
        };

        CLOUD.request(req, '/login', 'POST',
            UTILS.getRequestHeaders(req, {
                'Device-Identifier': UTILS.getDeviceId(req),
                'Device-Type': req.headers['user-agent']
            }),
            userData,
            function(err, reso, body) {
                if (!err) {
                    var user = UTILS.getResponse(body);
                    user['auth_token'] = self.token.encrypt(req, self.session.set(req, user));
                    return res.json(user);
                }
                return res.send(err);
            }
        );
    }, // login

    out: function(req, res, next) {
        var rurl = req.query.rurl || '/';
        self.session.remove(req);
        if (req.method == 'GET') return res.redirect(rurl);
        if (req.method == 'DELETE') return res.json({"error": false});
    }, // logout

    restore: function(req, res, next) {
        console.log('----- restore session -----');
        var orl  = req.originalUrl;
        var user = JSON.parse(self.token.decrypt(req, req.cookies.auth_token));
        CLOUD.request(req, '/'+ user.id +'/info', 'GET',
            UTILS.getRequestHeaders(req, {
                'Device-Identifier': UTILS.getDeviceId(req),
                'Device-Type': req.headers['user-agent'],
                'Session-Key': user.key
            }),
            '',
            function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
                    self.session.set(req, result);
                    res.redirect(orl);
                }
                delete req.cookies.auth_token;
            }
        );
    }

  },

  register: function(req, res, next) { // /users/register
    var userData = {
        'channel': req.body.channel,
        'email': req.body.email,
        'password': req.body.password,
        'name': req.body.name
    };

    CLOUD.request(req, '/register', 'POST',
      UTILS.getRequestHeaders(req, {
        'Device-Identifier': UTILS.getDeviceId(req),
        'Device-Type': req.headers['user-agent']
      }),
      userData,
      function(err, reso, body) {
        if (!err) {
          var result = UTILS.getResponse(body);
          return res.json(result);
        }
      }
    );
  }, // register

  get: {
    memberships: function(req, res, next) {
        var user = req.session.user;
        CLOUD.request(req, '/'+user.id+'/memberships', 'GET',
            UTILS.getRequestHeaders(req, {
                'Device-Identifier': UTILS.getDeviceId(req),
                'Device-Type': req.headers['user-agent'],
                'Session-Key': user.key
            }),
            '',
            function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
                    return res.json(result);
                }
            }
        );
    },

    purchases: function(req, res, next) {
        var user = req.session.user;
        CLOUD.request(req, '/logs/purchases?filter=points', 'GET',
            UTILS.getRequestHeaders(req, {
                'Device-Identifier': UTILS.getDeviceId(req),
                'Device-Type': req.headers['user-agent'],
                'Session-Key': user.key
            }),
            '',
            function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
                    return res.json(result);
                }
            }
        );
    }
  },

  token: {
    encrypt: function(req, u) {
      var token = null;
      if (u.id && u.key) {
        var cipher = crypto.createCipher(req.CONFIG.auth.algorithm, req.CONFIG.auth.password);
        token = JSON.stringify({"id":u.id,"key":u.key,"created_at":Date.now()});
        token = cipher.update(token, 'utf8', 'hex');
        token += cipher.final('hex');
      }
      return token;
    },

    decrypt: function(req, t) {
      var user = null;
      var decipher = crypto.createDecipher(req.CONFIG.auth.algorithm, req.CONFIG.auth.password);
      user = decipher.update(t, 'hex', 'utf8');
      user += decipher.final('utf8');
      return user;
    }
  },

}
