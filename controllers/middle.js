/* controllers/middle : middleware */

var sqlite3 = require('sqlite3').verbose();

var CONFIG = require('../config/default');
var USERS  = require('./users');

/*
var CONFIG = require('../libs/config')('./config/default.json'),
    Utils = require('../libs/utils'),
    Users = require('./users');
*/

exports.preload = function(req, res, next) {
	req.CONFIG = CONFIG;

	if (!req.session.user)
	  if (req.cookies.auth_token)
		  return USERS.log.restore(req, res, next);

	res.locals.CONFIG = CONFIG; // res!
	res.locals.USER = req.session.user;

	req.CATALOG_DB = new sqlite3.Database(CONFIG.db.catalog);

	next();
}
