/* controllers/showcase */
var uurl    = require('url');

var UTILS = require('../libs/utils');
var CLOUD = require('../libs/cloud');
var MARKET = require('../libs/market');

const PHONE_SUFFIX  = 'iphone-retina6';
const TABLET_SUFFIX = 'ipad-retina';

var self = module.exports = {

    products: {
        books: function(req, res, next) {
            var product_id = req.params.id;
            req.CATALOG_DB.get("SELECT * FROM products WHERE id = ?", product_id, function(err, row){
                if (!err && row) {
                    var item_ids = row.items.split(',');
                    var product = row;
                    product.items = item_ids;
                    return res.json({"product": product});
                }
            });
        },

        list: function(req, res, next) {
            req.CATALOG_DB.all("SELECT id, title, volume, membership, price, time FROM products ORDER BY id ASC", function(err, rows){
                if (!err && rows) {
                    return res.json({"products": rows});
                } else {
                    return res.send(err);
                }
            });
        }
    },

    market: {
        //= images
        images: function(req, res, next) {
            var device_type = (req.device.type == 'phone') ? PHONE_SUFFIX : TABLET_SUFFIX;
            MARKET.request(req, '/images/' + req.params.filename, 'POST',
                UTILS.getRequestHeaders(req, {
                    "Device-Identifier": UTILS.getDeviceId(req),
                    "Device-Type": device_type
                }),
                { "catalog_id": "Default" },
                function(err, reso, body) {
                    if (!err) return res.send(body);
                }
            );
        },
        //= covers
        covers: function(req, res, next) {
            var device_type = (req.device.type === 'phone') ? PHONE_SUFFIX : TABLET_SUFFIX;
            MARKET.request(req, '/covers/' + req.params.item_id, 'POST',
                UTILS.getRequestHeaders(req, {
                    "Device-Identifier": UTILS.getDeviceId(req),
                    "Device-Type": device_type
                }),
                '',
                function(err, reso, body) {
                    if (!err) {
                        var url_info = uurl.parse(body);
                        //console.log(url_info);
                        //return res.send(req.CONFIG.host.cds.bxps + url_info.path);
                        return res.send(url_info);
                    }
                }
            );
        }
    } // market

}
